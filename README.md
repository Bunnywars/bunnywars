# Bunny Wars
  
  A bunny killing things in the space.
  
## Build
  
### Prerequisites

  * node.js >= 5

### Instructions

1. `npm i`
2. `npm run build`
  
### Project structure

All development files are under `src` folder. This folder has its own sub-structure organizing files according to its type and
and semantic in this project (ie. All typescripts files are under `./src/typescripts` and typescripts describing entities under `./src/typescripts/entities`).
This folder must only contain files that would be modified. All external dependencies must need to be controlled by an external tool or script.

The only file outside a subfolder is `index.html` which is the only one of its kind.


  
